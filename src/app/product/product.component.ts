import { Component, OnInit } from '@angular/core';
import {  Output, EventEmitter } from '@angular/core';
import {Product} from './product';

@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Product>();
  @Output() editEvent = new EventEmitter<Product>();
  tempProduct:Product = {Pid:null,Pname:null,Cost:null,CategoryId:null};
  isEdit : boolean = false;
  editButtonText = 'Edit';
  product:Product;

  constructor() { }

   sendDelete(){
    this.deleteEvent.emit(this.product);
  }
 toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
    if(this.isEdit){
      this.product.Pid = this.tempProduct.Pid;
      this.product.Pname = this.tempProduct.Pname;
      this.product.Cost = this.tempProduct.Cost;
      this.product.CategoryId = this.tempProduct.CategoryId;
     } else {     
       this.editEvent.emit(this.product);
     }
 
  }
  

  ngOnInit() {
  }

}
