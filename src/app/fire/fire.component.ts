/*import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fire',
  templateUrl: './fire.component.html',
  styleUrls: ['./fire.component.css']
})
export class FireComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}*/

import { Component, OnInit } from '@angular/core';
import {AngularFire} from 'angularfire2'; 

@Component({
  selector: 'app-fire',
  templateUrl: './fire.component.html',
  styleUrls: ['./fire.component.css']
})
export class FireComponent implements OnInit {

  users;
  constructor(af:AngularFire) { 
    af.database.list('/users').subscribe(x => {this.users = x;
      console.log(this.users)})
  }      

  ngOnInit() {
  }

}
