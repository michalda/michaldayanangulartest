import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';

@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
   products;
   categoryName;
   newProducts;

  constructor(private _productsService: ProductsService) { }
  
  deleteProduct(product){

     this._productsService.deleteProduct(product);
     
  }
  
  editProduct(product){
     this._productsService.updateProduct(product); 
   }  

  ngOnInit() {
 
  /*    this._productsService.getProducts()
		    .subscribe(products => {this.products = products;});*/

              this._productsService.getProductsJoin()
		    .subscribe(products => {this.newProducts = products;});
  }

}
