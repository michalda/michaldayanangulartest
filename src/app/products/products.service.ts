import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {
   productsObservable;
   newProductsObservable;
constructor(private af: AngularFire) { }

   getProducts(){
	  this. productsObservable = this.af.database.list('/product');
    return this. productsObservable;
	}
     deleteProduct(product){
   this.af.database.object('/product/' + product.$key).remove()

  }
   updateProduct(product){
    let user1 = {pid:product.pid,pname:product.pname,cost:product.cost,categoryId:product.categoryId}
    console.log(user1);  
   this.af.database.object('/product/' + product.$key).update(user1)
  } 


   getProductsJoin(){ //לא לשכוח לקרוא לפונקציה בגו איניט
	  this.newProductsObservable = this.af.database.list('/product').map(
      products =>{
        products.map(
          product => {
            product.categoryName  = [];
             for(var p in product.category){ //שם תכונה category הנמצאת עבור כל מוצר
                product.categoryName.push(
                this.af.database.object('/category/' + p) // שם הטבלה שממנה רוצים לשלוף את הנתנון
              )
             }
          }
        );
        return products;
      }
    )
    return this.newProductsObservable;
	}
  

 

}
