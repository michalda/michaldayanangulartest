import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';

@Injectable()
export class PostsService {
/*posts= [
    {author :'John',content :'aaaaaaa',title:'dsfdss'},
    {author :'lolo',content :'bbbbbb',title:'dfdsf'},
    {author :'jojo',content :'ccccc',title:'jhgjhgj'},
     {author :'daty',content :'bbbbbb',title:'dfdsf'}
    ]*/
  //   private _url = "http://jsonplaceholder.typicode.com/posts";
    postsObservable;
     constructor(private af: AngularFire) { }
 addPost(post){
          this.postsObservable.push(post);
 }
 deletePost(post){
   this.af.database.object('/posts/' + post.$key).remove()

  }
    updatePost(post){
    let post1 = {Id:post.id,Title:post.title,Body:post.body}
    console.log(post1);  
   this.af.database.object('/posts/' + post.$key).update(post1)
  }  
  
    getPosts(){
      	/*	return this._http.get(this._url)
			.map(res => res.json()).delay(2000);*/
    /* this.postsObservable = this.af.database.list('/posts');
    return this.postsObservable;*/
     this.postsObservable = this.af.database.list('/posts').map(
      posts =>{
        posts.map(
          post => {
            post.userName = [];
            for(var p in post.users){
                post.userName.push(
                this.af.database.object('/users/' + p)
              )
            }
          }
        );
        return posts;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.postsObservable;
	
	}



}
