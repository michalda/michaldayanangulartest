import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Invoice} from '../invoice/invoice'

@Component({
  selector: 'jce-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {
  @Output() invoiceAddedEvent = new EventEmitter<Invoice>();
  invoice:Invoice = {
   ProductName:'',
   Amount: ''
 
 };

  constructor() { }
  onSubmit(form:NgForm){
     
    this.invoiceAddedEvent.emit(this.invoice);
    this.invoice = {
        ProductName:'',
        Amount: ''
 
    }
  }

  ngOnInit() {
  }

}
