import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: black; 
         color: red;
    }     
  `]
})
export class InvoicesComponent implements OnInit {
  invoices;
  isLoading = true;
  currentInvoice ;
  //currentInvoice = this.invoices[0];

 select(invoice){
		this.currentInvoice = invoice; 
 }
 

  constructor(private _invoicesService: InvoicesService) { }
   addInvoice(invoice){
    this.invoices.push(invoice)
    this._invoicesService.addInvoice(invoice);
  }

  ngOnInit() {
    this._invoicesService.getInvoices()
		    .subscribe(invoices => {this.invoices = invoices; 
                                  this.isLoading = false});
  }

}
